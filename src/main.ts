import { createApp } from 'vue'
import App from './App.vue'
import { OhVueIcon,addIcons } from "oh-vue-icons";
import { BiFileArrowDownFill,GiRamProfile  } from "oh-vue-icons/icons";

addIcons(BiFileArrowDownFill)
addIcons(GiRamProfile)

const app=createApp(App)
app.component('v-icon',OhVueIcon)
app.mount('#app')
